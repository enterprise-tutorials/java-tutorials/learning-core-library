package com.michael.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class EmployeeTest {

	@Autowired
	private Employee employee;

	@Before
	public void setup() {
		if( employee == null ) {
			employee = new Employee();
			employee.setId(1);
			employee.setName("Test Name");
			employee.setAge(100);
		}
	}

	@Test
	public void testNotNull() {
		assertNotNull(employee);
	}

}
