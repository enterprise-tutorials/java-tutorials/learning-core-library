package com.michael.core;

import lombok.Data;
import java.util.List;

/**
 * Company DTO.
 */
@Data
public class Company {

    private String id;
    private String name;
    private String address;
    private String country;

    private List<Employee> employees;
}
