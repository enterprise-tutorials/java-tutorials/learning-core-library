package com.michael.core;

/**
 * String utility class.
 */
public class StringUtils {

    public void splitNameAndVersion(String strToSplit) {

        System.out.println("String to Split" + strToSplit);

        // This section below is to separate player name from version
        // ex 1: "JW Player 8.0" should be "JW Player" and "8.0"
        // ex 2: "Brightcove Player 6" should be "Brightcove Player" and "6"
        // TODO: The logic written here is just a hack. Ideally we should
        //       call the beacon-engine API to get the player name & version

        Integer lastIndex = strToSplit.lastIndexOf(" ");
        String stringVersion  = strToSplit.substring(lastIndex + 1, strToSplit.length());
        String stringName = strToSplit.substring(0, lastIndex).toLowerCase().replaceAll("\\s", "");

        System.out.println("String split as - name [" + stringName + "], version [" + stringVersion + "]");


    }
}
