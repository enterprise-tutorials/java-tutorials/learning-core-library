package com.michael.core;

import lombok.Data;

/**
 * Employee DTO.
 */
@Data
public class Employee {

    private int id;
    private String name;
    private int age;
    private Role role;
    private Company company;
}
