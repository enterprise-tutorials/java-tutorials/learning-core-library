package com.michael.core;

import lombok.Data;

/**
 * Role DTO.
 */
@Data
public class Role {

    private String id;
    private String name;
    private String description;

}
